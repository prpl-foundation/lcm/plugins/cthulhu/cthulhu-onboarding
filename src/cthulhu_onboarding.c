/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <dirent.h>

#include <amxc/amxc.h>
#include <amxm/amxm.h>
#include <amxp/amxp.h>
#include <yajl/yajl_gen.h>
#include <amxj/amxj_variant.h>
#include <amxd/amxd_dm.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>
#include <cthulhu/cthulhu_defines_plugin.h>
#include <cthulhu/cthulhu_helpers.h>
#include <cthulhu/cthulhu_config.h>

#include <lcm/lcm_assert.h>
#include <lcm/amxc_data.h>
#include <debug/sahtrace.h>

#include "cthulhu_onboarding.h"

#define ME "cthulhu_onboarding"


static amxm_shared_object_t* so = NULL;
static amxm_module_t* mod = NULL;
static amxd_dm_t* dm = NULL;
static cthulhu_prestartup_config_t* prestartup_config = NULL;

static int cthulhu_onboarding_init(UNUSED const char* function_name, UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret) {
    dm = (amxd_dm_t*) GET_DATA(args, CTHULHU_PLUGIN_ARG_DATAMODEL);
    prestartup_config = (cthulhu_prestartup_config_t*) GET_DATA(args, CTHULHU_PLUGIN_ARG_PRESTART_CONFIG);

    ASSERT_NOT_NULL(dm, return -1);
    ASSERT_NOT_NULL(prestartup_config, return -1);

    return 0;
}

static amxc_var_t* read_file(const char* json_file_path) {
    int read_length = 0;
    amxc_var_t* ret = NULL;
    variant_json_t* reader = NULL;
    int fd = open(json_file_path, O_RDONLY);
    ASSERT_TRUE(fd >= 0, goto exit, "Cannot open '%s': (%d) %s", json_file_path, errno, strerror(errno));

    amxj_reader_new(&reader);
    if(reader == NULL) {
        goto exitclean;
    }

    do{
        read_length = amxj_read(reader, fd);
    } while(read_length > 0);

    ret = amxj_reader_result(reader);
    amxj_reader_delete(&reader);
exitclean:
    close(fd);
exit:
    return ret;
}

static int read_all(const char* config_dir, cthulhu_prestartup_config_t* config) {
    int ret = 0;
    DIR* dir_ptr;
    struct dirent* entry;
    amxc_string_t path;

    ASSERT_NOT_NULL((dir_ptr = opendir(config_dir)), return -1, "Cannot open onboarding dir: '%s'", config_dir);

    amxc_string_init(&path, 256);
    while((entry = readdir(dir_ptr)) != NULL) {
        if(entry->d_name[0] != '.') {
            struct stat info;
            amxc_string_setf(&path, "%s/%s", config_dir, entry->d_name);
            const char* full = amxc_string_get(&path, 0);
            if(stat(full, &info) != 0) {
                SAH_TRACEZ_ERROR(ME, "Cannot stat() on '%s' (%d): %s", full, errno, strerror(errno));
                ret = -2;
            } else if((S_ISREG(info.st_mode)) || (S_ISLNK(info.st_mode))) {
                SAH_TRACEZ_INFO(ME, "Using onboarding file %s", full);
                amxc_var_t* instance = amxc_var_add(amxc_llist_t, config->ctr_create, NULL);
                amxc_var_t* ctr = read_file(full);
                amxc_var_move(instance, ctr);
            }
        }
    }

    amxc_string_clean(&path);
    return ret;
}

static int cthulhu_onboarding_prestart(UNUSED const char* function_name, UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret) {
    int res = -1;
    char* onboarding_file = NULL;
    char* onboarding_location = NULL;

    ASSERT_NOT_NULL(prestartup_config, return res);

    amxd_object_t* cthulhu_config = amxd_dm_findf(dm, CTHULHU_DM_CONFIG ".");
    onboarding_file = amxd_object_get_cstring_t(cthulhu_config, CTHULHU_ONBOARDING_FILE, NULL);

    if(!(access(onboarding_file, F_OK) == 0)) {
        int fd = creat(onboarding_file, 0660);
        onboarding_location = amxd_object_get_cstring_t(cthulhu_config, CTHULHU_ONBOARDING_LOCATION, NULL);
        SAH_TRACEZ_NOTICE(ME, "'%s' does not exist, will onboard from: %s", onboarding_file, onboarding_location);
        read_all(onboarding_location, prestartup_config);
        close(fd);
    } else {
        SAH_TRACEZ_NOTICE(ME, "'%s' exists. Will not onboard!", onboarding_file);
    }

    free(onboarding_file);
    free(onboarding_location);

    res = 0;
    return res;
}

static int register_function(const char* const func_name, amxm_callback_t cb) {
    ASSERT_SUCCESS(amxm_module_add_function(mod, func_name, cb), return -1, "Could not add function [%s]", func_name);
    return 0;
}

AMXM_CONSTRUCTOR module_init(void) {
    SAH_TRACEZ_INFO(ME, ">>> Cthulhu Onboarding constructor\n");
    int ret = 1;

    so = amxm_so_get_current();
    ASSERT_NOT_NULL(so, goto exit, "Could not get shared object");
    ASSERT_SUCCESS(amxm_module_register(&mod, so, CTHULHU_PLUGIN), goto exit, "Could not register module %s", CTHULHU_PLUGIN);
    ASSERT_SUCCESS(register_function(CTHULHU_PLUGIN_INIT, cthulhu_onboarding_init), goto exit);
    ASSERT_SUCCESS(register_function(CTHULHU_PLUGIN_ODL_LOADED, cthulhu_onboarding_prestart), goto exit);

    ret = 0;
exit:
    SAH_TRACEZ_INFO(ME, "<<< Cthulhu Onboarding constructor\n");
    return ret;
}

AMXM_DESTRUCTOR module_exit(void) {
    SAH_TRACEZ_INFO(ME, ">>> Cthulhu Onboarding destructor\n");
    SAH_TRACEZ_INFO(ME, "<<< Cthulhu Onboarding destructor\n");
    return 0;
}